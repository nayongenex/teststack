package middelwares

import "net/http"

func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "applicaton/json")
		next(w, r)
	}
}
