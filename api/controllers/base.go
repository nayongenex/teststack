package controllers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
	"net/http"
)

type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

func (server *Server) Initialize() {
	// database connection details
	server.Router = mux.NewRouter()
	server.InitialRoutes()
}

func (server *Server) Run(addr string) {
	fmt.Println("Listen to the port ", addr)
	log.Fatal(http.ListenAndServe(addr, server.Router))
}
