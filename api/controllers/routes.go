package controllers

import middelwares "bitbucket.org/nayongenex/teststack/api/middlewares"

func (s *Server) InitialRoutes() {
	s.Router.HandleFunc("/", middelwares.SetMiddlewareJSON(s.Home)).Methods("GET")
}
