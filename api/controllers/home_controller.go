package controllers

import "net/http"

import "bitbucket.org/nayongenex/teststack/api/responses"

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Hello This is awoseme API")
}
