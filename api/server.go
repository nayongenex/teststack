package api

import "bitbucket.org/nayongenex/teststack/api/controllers"

var server = controllers.Server{}

func Run() {
	server.Initialize()
	server.Run(":8080")
}
